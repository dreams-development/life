# Life
## Articles
### Productivity
- [These 10 Productivity Techniques Will Make You More Successful](https://medium.com/personal-growth/these-10-productivity-techniques-will-make-you-more-successful-1f111400f7a4)
- [How To Start — When You’ve Procrastinated On Your Goals For Too Long](https://medium.com/personal-growth/how-to-start-when-youve-procrastinated-on-your-goals-for-too-long-cee0ced9504c)
- [This Exercise Will Help You To Stop Wasting Your Time](https://medium.com/darius-foroux/this-exercise-will-help-you-to-wasting-your-time-447cce156205)
- [Hard Work is Not Enough](https://medium.com/thrive-global/if-all-you-do-in-life-is-work-really-hard-you-are-never-going-to-be-successful-1550f58bf127)
- [14 Strategies To Accelerate Your Personal Growth By 1,000%](https://medium.com/@benjaminhardy/how-to-continually-grow-10x-faster-than-expected-personally-and-professionally-f343a7fef131)
- [You Make Or Break Your Life Between 5-7 AM](https://medium.com/thrive-global/you-make-or-break-your-life-between-5-7-am-70e2717f9e67)
- [The Myth of Laziness](https://medium.com/@MikeSturm/the-myth-of-laziness-6a91889329f7)
- [How to Immediately Refresh Your Brain to Restore High-Level Thinking](https://medium.com/swlh/how-to-immediately-refresh-your-brain-to-restore-high-level-thinking-c5550b9c2365)
- [You Won’t Feel Motivated Every Single Day. Focus On This Instead](https://medium.com/personal-growth/you-wont-feel-motivated-every-single-day-focus-on-this-instead-33e35a9b2f77)
- [Why You Don’t Need To Read Those Productivity Guides](https://medium.com/the-polymath-project/why-you-dont-need-to-read-those-productivity-guides-347fe02cc196)
- [Мозг и отдых. Как устроить мозгу перезагрузку и придать сил](https://update.com.ua/likbez_tag925/mozg-i-otdykh-kak-ustroit-mozgu-perezagruzku-i-pridat-sil_n3760)
- [Как обмануть мозг и эффективно учиться](https://proglib.io/p/effective-learning/)

### Life
- [How To Change Your Life In 30 Days](https://medium.com/thrive-global/how-to-change-your-life-in-30-days-70b0ca64f701)
- [15 Things You Need To Be Successful That Your Loser Ass Friends Don’t Do.](https://medium.com/the-mission/15-things-you-need-to-be-successful-that-your-loser-ass-friends-dont-do-a3152dfd42a3)
- [“I don’t know“](https://medium.com/mule-design/i-dont-know-15a6aca3bee2)
- [How to Debug Any Problem](https://hackernoon.com/how-to-debug-any-problem-ac6f8a867fae)
- [To Have What You Want, You Must Give-Up What’s Holding You Back](https://medium.com/@benjaminhardy/to-have-what-you-want-you-must-give-up-whats-holding-you-back-65275f844a5a)
- [Психология убеждения. Как убеждать других и уметь распознавать манипуляции](https://habr.com/post/316784/)
- [12 Questions That Will Change Your Life](https://medium.com/thrive-global/12-questions-that-will-change-your-life-9d281c1e4704)
- [Tell Me What You Say “Yes” To, And I’ll Tell You Who You Are](https://medium.com/thrive-global/you-are-what-you-say-yes-to-60457c4d26d3)
- [Thinking in Levels (How to Dig Deeper And Think Better)](https://medium.com/personal-growth/thinking-in-levels-how-to-dig-deeper-and-think-better-8909afbe4fed)
- [Felling Stuck in Life? How To Take Your Life From Stuck to Awesome](https://medium.com/@alltopstartups/felling-stuck-in-life-how-to-take-your-life-from-stuck-to-awesome-3e421deca9fc)
- [The First Step to Getting What You Want is Making the Ask](https://medium.com/personal-growth/the-first-step-to-getting-what-you-want-is-making-the-ask-cc289bf0e44b)
- [The Next Step to Getting What You Want — Be Specific and Clear](https://medium.com/swlh/the-next-step-to-getting-what-you-want-be-specific-and-clear-c43c433b41ec)
- **[Want To Become A Multi-Millionaire? Do These 15 Things Immediately.](https://medium.com/thrive-global/want-to-become-a-multi-millionaire-do-these-15-things-immediately-e1e779a6978f)**
- [Transform Your Life By Transforming Your Habits](https://medium.com/darius-foroux/transform-your-life-by-transforming-your-habits-77da786feda7)
- [Small Wins, Marginal Gains: That’s How You Change Behavior in The Long Term](https://medium.com/personal-growth/small-wins-marginal-gains-thats-how-you-change-behavior-in-the-long-term-b3b94036d980)
- [Marriage made a lot of sense once upon a time](https://medium.com/personal-growth/does-marriage-even-make-sense-anymore-70e10f4d8c18)
- [The Most Important Skill Nobody Taught You](https://medium.com/personal-growth/the-most-important-skill-nobody-taught-you-9b162377ab77)
- [This 15–30 Minute Routine Makes Your Brain Healthier, Younger, And Smarter](https://medium.com/thrive-global/this-fun-15-30-minute-strategy-will-make-your-brain-healthier-and-learn-faster-4a67e82dd81)
- [5 Questions You Need To Ask Yourself Constantly](https://medium.com/@tom.stevenson78/5-questions-you-need-to-ask-yourself-constantly-ab01dc9066d8)
- [Everything I Have Learned in 500 Words](https://medium.com/@ztrana/everything-i-have-learned-in-500-words-6c4a5ccbc8e1)
- [Почему мы не становимся такими, какими хотим быть?](http://telegra.ph/Pochemu-my-ne-stanovimsya-takimi-kakimi-hotim-byt-02-21)

### Brain
- [Эффективный способ повысить умственную работоспособность](https://habr.com/post/25688/)
- [Как накормить мозг программиста… или feed your brain](https://habr.com/post/239793/)
- [Как прокачивать мозг](https://habr.com/post/95004/)
- [Модели реальности и их роль в жизни людей](https://habr.com/post/91904/)
- [Как на самом деле работает мозг](https://habr.com/post/95438/)
- [Зачем на самом деле нужен мозг](https://habr.com/post/111707/)
- [Overclock мозга или Внутренняя виртуализация сознания](https://habr.com/post/219819/)

### Programmers life
- [The Problem You Solve Is More Important Than The Code You Write](https://levelup.gitconnected.com/the-problem-you-solve-is-more-important-than-the-code-you-write-d0e5493132c6)
- [The Essential Guide to Take-home Coding Challenges](https://medium.freecodecamp.org/the-essential-guide-to-take-home-coding-challenges-a0e746220dd7)
- [Advice From A 19 Year Old Girl & Software Developer](https://medium.com/@lydiahallie/advice-from-a-19-y-o-girl-software-developer-88737bcc6be5)
- [How to think like a programmer](https://medium.freecodecamp.org/how-to-think-like-a-programmer-3ae955d414cd)
- [Why so Many Developers Quit Before Ever Getting a Job. Please — don’t.](https://medium.freecodecamp.org/why-so-many-developers-quit-before-ever-getting-a-job-please-dont-1c0fd6459e5e)
- [Sprint: как проверить любую бизнес-идею всего за 5 дней](https://habr.com/company/makeright/blog/318308/)
- [Почему программировать легко](https://habr.com/company/infopulse/blog/263331/)
- [Кто мы и что мы здесь делаем](https://habr.com/company/infopulse/blog/150252/)

### Tech
- [Composing Software: An Introduction](https://medium.com/javascript-scene/composing-software-an-introduction-27b72500d6ea)
- [The Rise and Fall and Rise of Functional Programming (Composing Software)](https://medium.com/javascript-scene/the-rise-and-fall-and-rise-of-functional-programming-composable-software-c2d91b424c8c)
- [How to Create a Million-Dollar Business This Weekend (Examples: AppSumo, Mint, Chihuahuas)](https://medium.com/@timferriss/how-to-create-a-million-dollar-business-this-weekend-examples-appsumo-mint-chihuahuas-fae4dab10842)

### Job
- [7 Interview Questions That Can Help You Land Your Dream Job](https://www.fastcompany.com/3053473/7-interview-questions-that-can-help-you-land-your-dream-job)
- [These are the questions you want to ask at that job interview.](https://medium.freecodecamp.org/vital-questions-to-ask-an-interviewer-bonus-question-at-the-end-264bc2caff8d)

